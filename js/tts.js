function start() {
    var progress = document.getElementById("progress");
    progress.hidden = false;

    if(window.speechSynthesis) {
        var text = document.getElementById("text").value;
        var lang = document.getElementById("lang").value;
        var tts = new SpeechSynthesisUtterance(text);

        tts.speed = 1.1;
        tts.pitch = 1;
        tts.lang = lang;
    
        console.log("About to say: " + text);
        window.speechSynthesis.speak(tts);
    } else {
        alert(Speaker_i18n.oldBrowser);
    }

    progress.hidden = true;
}